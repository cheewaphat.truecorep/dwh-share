# install hdfs-client

On the external host download the CDH repo file to the `/etc/yum.repos.d/` directory:

## 1. check os version and download repo for your version

```shell
cat /etc/redhat-release
#for example os is centos 7
```
![image](/uploads/5ebcb35895c95e9b50f34aff00865fde/image.png)

## 2. copy CDH repo to `/etc/yum.repos.d/`

```shell
-- download cdh5-repo
curl -O https://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/cloudera-cdh5.repo
```



## 3. Edit the base URL in the cloudera-cdh5.repo file to install the CDH version (otherwise it will install the latest). For example, to install the 5.7.1 hadoop-client, update the baseurl to:`https://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/5/`

```shell
baseurl=https://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/5/
```

![image](/uploads/2b4f1d0bf2bb6fa16182df2277b4707e/image.png)

>Install the hadoop-client rpm:

```shell
yum clean all
yum install hadoop-client
```
![image](/uploads/4c97de43c564f2d107c235996706d6f4/image.png)

## 4. Copy all the unzipped configuration files to /etc/hadoop/conf. 

[hdfs-clientconfig for Hadoop-200TB Sandbox](/uploads/8c37ed6cbd4ce3e919c0b710f9accbe5/hdfs-clientconfig.zip)

Example:
```shell
cp *  /etc/hadoop/conf
```
![image](/uploads/3e26972fcbbfd37ed653e7ae0d5eaf7a/image.png)

Run hadoop commands. Example:
```shell
sudo -u hdfs hadoop fs -ls
```
![image](/uploads/40f016ceadd51d62026890750f29a669/image.png)