# How to Export data

#### Create temp table on hadoop (IMPALA)

```sql
drop table if exists rwzcistemp.GEOSPACE_CIC_parin;

CREATE TABLE rwzcistemp.GEOSPACE_CIC_parin stored AS parquet AS
SELECT CAST(aa.tm_key_day AS INT ) tm_key_day,
       aa.product,
       aa.subject_id,
       aa.subject_name,
       aa.service_id,
       aa.lat,
       aa.lng,
       aa.scab,
       aa.turf_id,
       aa.ccaatt,
       aa.orgid_aa,
       aa.orgid_h,
       aa.orgid_r,
       aa.orgid_g,
       aa.orgid_p,
       aa.data_as_of,
       aa.prod_ctgry,
       aa.prod_typ_nm,
       aa.resre_typ,
       aa.dwh_actv_tm_key_day,
       aa.dwh_prod_stat_dscr,
       aa.cic_days,
       aa.cic_grp,
       from_timestamp(aa.cntrct_exp_dt_cal,'yyyy-MM-dd HH:mm:ss') cntrct_exp_dt_cal,
       aa.invc_amt,
       REGEXP_REPLACE(aa.convergence_cd, '[^0-9A-Za-z\/ ]+', '') convergence_cd,
--       aa.convergence_cd,
--       aa.convergence_cd convergence_cd_orig,
--       REPLACE(aa.convergence_cd,'\n','') convergence_cd_nl,
--       base64encode(aa.convergence_cd) convergence_cd,
--       REPLACE(TRIM(REPLACE(aa.convergence_cd,'\r\n','')),'\n','') convergence_cd,
--       trim(regexp_extract(aa.convergence_cd, '.*?(\\w+)', 0)) convergence_cd,
       aa.full_th_nm,
       aa.contact_number,
       aa.complaints_no,
       aa.network_exp acct_typ,
       aa.mktg_cd,
       aa.sl_ccaatt_cd,
       aa.ref_prtnr_cd,
       aa.dwh_sl_cd,
       aa.sl_prtnr_chnl_nm,
       aa.techncn_cd,
       aa.techncn_nm,
       aa.card_type,
       aa.compaign,
       aa.nw_exp_grade,
       aa.prod_typ_cd
FROM
  (
  SELECT from_timestamp(a.start_dt, 'yyyymmdd') AS TM_KEY_DAY,
          CASE
              WHEN p.prod_typ_cd = 1 THEN 'POSTPAID'
              WHEN p.prod_typ_cd = 2 THEN 'BOARDBAND'
              ELSE 'OTHER'
          END AS Product,
          CASE
              WHEN p.prod_typ_cd = 1 THEN 'H012'
              WHEN p.prod_typ_cd = 2 THEN 'H013'
              ELSE 'OTHER'
          END AS SUBJECT_ID,
          CASE
              WHEN p.prod_typ_cd = 1 THEN 'POSTPAID CIC'
              WHEN p.prod_typ_cd = 2 THEN 'BOARDBAND CIC'
              ELSE 'OTHER'
          END AS SUBJECT_NAME,
          A.ref_resre_nbr AS SERVICE_ID,
          O.LATITUDE AS lat,
          O.LONGITUDE AS lng,
          o.SCAB_CODE AS scab,
          '    ' AS TURF_ID,
          E.DWH_CCAATT_CD AS CCAATT,
          MM.ORGID_AA,
          MM.ORGID_H,
          MM.ORGID_R,
          MM.ORGID_G,
          MM.ORGID_P,
          from_timestamp(NOW(), 'yyyymmdd') AS DATA_AS_OF,
          P.PROD_CTGRY,
          P.PROD_TYP_NM,
          P.RESRE_TYP,
          P.DWH_ACTV_TM_KEY_DAY,
          P.DWH_PROD_STAT_DSCR,
          A.cic_days,
          a.cic_grp,
          a.cntrct_exp_dt_cal,
          I.INVC_AMT,
          p.convgnt_cd AS CONVERGENCE_CD,
          p.full_th_nm,
          A.ref_resre_nbr AS contact_number,
          CAST(NULL AS STRING ) AS complaints_no,
          ' ' network_exp,
              B.acct_typ,
              P.MKTG_CD,
              E.SL_CCAATT_CD,
              O.REF_PRTNR_CD,
              O.DWH_SL_CD,
              O.SL_PRTNR_CHNL_NM,
              O.TECHNCN_CD,
              O.TECHNCN_NM,
              CASE
                  WHEN SUBSTR(B.ACCT_PRIORITY, 1, 1) IN ('V',
                                                         'A',
                                                         'B',
                                                         'P') THEN 'BLACK'
                  WHEN SUBSTR(B.ACCT_PRIORITY, 1, 1) = 'Y' THEN 'GREEN'
                  WHEN SUBSTR(B.ACCT_PRIORITY, 1, 1) = 'O' THEN 'BLUE'
                  WHEN SUBSTR(B.ACCT_PRIORITY, 1, 1) = 'M' THEN 'METAL'
                  WHEN SUBSTR(B.ACCT_PRIORITY, 1, 1) = 'G' THEN 'RED'
                  WHEN SUBSTR(B.ACCT_PRIORITY, 1, 1) = 'W' THEN 'WHITE'
                  ELSE 'OTHER'
              END AS CARD_TYPE,
              '   ' AS COMPAIGN,
              '  ' AS NW_EXP_GRADE,
              ROW_NUMBER() OVER(PARTITION BY A.PROD_KEY
                                ORDER BY I.AS_OF_MTH_KEY DESC ,I.INVC_KEY DESC) ROW_NUM,
                           p.prod_typ_cd
   FROM
     (
     		SELECT DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) AS cic_days,
             CASE
                 WHEN DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) <= 30 THEN '30'
                 WHEN DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) >= 31
                      AND DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) <= 60 THEN '31 - 60'
                 WHEN DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) >= 61
                      AND DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) <= 90 THEN '61 - 90'
                 WHEN DATEDIFF(prp.cntrct_exp_dt_cal, NOW()) >= 91 THEN '91'
                 ELSE 'other'
             END AS cic_grp,
             prp.*
      FROM
        (
        		SELECT 
        		prod_key,
                ref_resre_nbr,
                prod_nbr,
                prod_ctgry,
                prod_sub_typ,
                cust_bagg_key,
                cust_acct_key,
                cust_acct_nbr,
                prod_ofr_key,
                soc_cd,
                prod_ofr_nm,
                prod_ofr_dscr,
                src_typ,
                dwh_soc_typ,
                cntrct_seq_no,
                start_dt,
                end_dt,
                cntrct_exp_dt,
                CASE
                    WHEN nvl(end_dt, from_unixtime(unix_timestamp('2099-01-01', 'yyyy-MM-dd'), 'yyyy-MM-dd')) <= ADD_MONTHS(start_dt, cast(REGEXP_REPLACE(nvl(cntrct_term, '0'), '[^0-9]', '0') AS int)) THEN end_dt
                    ELSE ADD_MONTHS(start_dt, cast(REGEXP_REPLACE(nvl(cntrct_term, '0'), '[^0-9]', '0') AS int))
                END AS cntrct_exp_dt_cal,
                cntrct_term,
                cntrct_fee ,
                ROW_NUMBER() OVER (PARTITION BY prps.prod_key
                                   ORDER BY cast(REGEXP_REPLACE(nvl(cntrct_term, '0'), '[^0-9]', '0') AS int) DESC, (CASE
                                                                                                                         WHEN nvl(prps.end_dt, TO_TIMESTAMP('01012099', 'ddmmyyyy')) <= ADD_MONTHS(start_dt, cast(REGEXP_REPLACE(nvl(cntrct_term, '0'), '[^0-9]', '0') AS int)) THEN end_dt
                                                                                                                         ELSE ADD_MONTHS(start_dt, cast(REGEXP_REPLACE(nvl(cntrct_term, '0'), '[^0-9]', '0') AS int))
                                                                                                                     END) DESC) RNK
         FROM rfzacs.FCT_PP_PRPSTN prps
         WHERE dwh_soc_typ = 'PPS'
           AND ADD_MONTHS(start_dt, cast(REGEXP_REPLACE(nvl(cntrct_term, '0'), '[^0-9]', '0') AS int)) >= NOW()
           AND nvl(end_dt, from_unixtime(unix_timestamp('2099-01-01', 'yyyy-MM-dd'), 'yyyy-MM-dd')) >= NOW() ) prp
      WHERE RNK = 1 ) A
   INNER JOIN rfzacs.DIM_PROD P ON A.PROD_KEY = P.PROD_KEY
   INNER JOIN rfzacs.DIM_PROD_EXT E ON A.PROD_KEY = E.PROD_KEY
   AND P.PROD_KEY = E.PROD_KEY
   INNER JOIN rfzacs.DIM_ACCT B ON P.CUST_ACCT_KEY = B.CUST_ACCT_KEY
   INNER JOIN rfzacs.DIM_ORDR_ACTVTN O ON O.PROD_KEY = P.ORIG_PROD_KEY
   INNER JOIN rfzacs.FCT_INVC_PROD I ON A.PROD_KEY = I.PROD_KEY
   LEFT JOIN
     (
     	SELECT DISTINCT m.CCAATT,
                      '         ' AS ORGID_AA,
                      m.ORGID_H,
                      m.ORGID_R,
                      m.ORGID_G,
                      m.ORGID_P --A.ORGID_AA,A.ORG_LEVEL_AA,ORG_LEVEL_AA_NAME,A.REMARK,SPECIAL_AA

      FROM rfzcis.dim_mooc_area M
      WHERE m.TEAM_CODE <> 'ไม่ระบุ'
        AND (m.REMARK <> 'Dummy'
             OR m.SPECIAL_AA = 'Normal')) mm ON E.DWH_CCAATT_CD = MM.CCAATT --where p.prod_typ_cd = 1
) AA
WHERE AA.ROW_NUM = 1;
```

## ORACLE TARGET TABLE

```sql
DROP TABLE GEOSPCSBOX.GEOSPACE_CIC
CREATE TABLE GEOSPCSBOX.GEOSPACE_CIC
(
  tm_key_day NUMBER(8),
  product VARCHAR2(1000),
  subject_id VARCHAR2(1000),
  subject_name VARCHAR2(1000),
  service_id VARCHAR2(100),
  lat NUMBER(18,8),
  lng NUMBER(18,8),
  scab VARCHAR2(90),
  turf_id VARCHAR2(1000),
  ccaatt VARCHAR2(1000),
  orgid_aa VARCHAR2(1000),
  orgid_h VARCHAR2(150),
  orgid_r VARCHAR2(150),
  orgid_g VARCHAR2(150),
  orgid_p VARCHAR2(150),
  data_as_of VARCHAR2(1000),
  prod_ctgry VARCHAR2(300),
  prod_typ_nm VARCHAR2(720),
  resre_typ VARCHAR2(300),
  dwh_actv_tm_key_day NUMBER,
  dwh_prod_stat_dscr VARCHAR2(300),
  cic_days INT,
  cic_grp VARCHAR2(1000),
  cntrct_exp_dt_cal TIMESTAMP,
  invc_amt NUMBER(12,4),
  convergence_cd VARCHAR2(1000),
  full_th_nm VARCHAR2(1200),
  contact_number VARCHAR2(100),
  complaints_no VARCHAR2(1000),
  acct_typ VARCHAR2(1000),
  mktg_cd VARCHAR2(90),
  sl_ccaatt_cd VARCHAR2(1000),
  ref_prtnr_cd VARCHAR2(150),
  dwh_sl_cd VARCHAR2(150),
  sl_prtnr_chnl_nm VARCHAR2(300),
  techncn_cd VARCHAR2(300),
  techncn_nm VARCHAR2(1200),
  card_type VARCHAR2(1000),
  compaign VARCHAR2(1000),
  nw_exp_grade VARCHAR2(1000),
  prod_typ_cd NUMBER(1,0)

);

CREATE INDEX IDX_GEOSPCSBOX_GEOSPACE_CIC_01 ON GEOSPCSBOX.GEOSPACE_CIC(turf_id);
CREATE INDEX IDX_GEOSPCSBOX_GEOSPACE_CIC_02 ON GEOSPCSBOX.GEOSPACE_CIC(tm_key_day);
  

```

## PYSPARK2
```python
table = "rwzcistemp.geospace_cic_parin"
dest = "/tmp/dwh-etl/{table}".format(table=table)
df = spark.sql( 'select * from {table}'.format(table=table) )
df.printSchema()
df.repartition(8)
df.coalesce(8).write.mode('overwrite').format("com.databricks.spark.csv").option('header',False).option('delimiter',"|").save(dest)
```


## Sqoop Export
```sh

sqoop export -Dmapreduce.job.queuename="dwh.export" -D oraoop.oracle.rac.service.name=TDMDBPR \
    -Doraoop.chunk.method=rowid -Doracle.row.fetch.size=4096 \
    -D mapreduce.map.memory.mb=8192 -D mapreduce.map.java.opts=-xmx4096m \
    -Dmapreduce.map.java.opts='-Duser.timezone=ICT' \
    --outdir "/tmp/sqoop/" \
    --class-name "GEOSPACE_CIC_PARIN_EXPORT" \
    --connect "jdbc:oracle:thin:@172.16.59.27:1521/TDMDBPR" \
    --username "GEOSPCSBOX" -P \
    --table "GEOSPCSBOX.GEOSPACE_CIC" \
    --export-dir "/tmp/dwh-etl/rwzcistemp.geospace_cic_parin/" \
    --input-fields-terminated-by '|' \
    --lines-terminated-by '\n' \
    --input-null-string "\\\\N" --input-null-non-string "\\\\N" \
    -m8
```